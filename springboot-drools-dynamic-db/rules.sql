DROP TABLE IF EXISTS `rules`;
CREATE TABLE `rules` (
  `id` int(11) NOT NULL,
  `rules` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `rules` VALUES ('1', 'package com.jeegem.drools; import com.jeegem.drools.bean.Message; rule "Hello World Message " when $message:Message (status == "0") then $message.setContent("hello, Drools Message   !");   System.out.println("hello, Drools Message!"); end');
