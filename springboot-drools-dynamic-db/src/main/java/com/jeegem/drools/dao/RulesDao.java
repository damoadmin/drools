package com.jeegem.drools.dao;

import com.jeegem.drools.bean.Rules;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface RulesDao {
     Rules getById (@Param("id")Integer id);
}
