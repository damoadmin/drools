package com.jeegem.drools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroolsStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(DroolsStartApplication.class, args);
    }

}
